package org.fasttrackit;

import jdk.internal.icu.impl.BMPSet;

public class DemoShop {
    public static void main(String[] args) {
        System.out.println("- = Demo Shop = -");
        System.out.println("1. User can log in with valid credentials.");
        HomePage homePage = new HomePage();
        homePage.openHomePage();
        Header header = homePage.header;// -> Create Header object!
        homePage.header.clickOnTheLoginButton();
        ModalDialog modalDialog = new ModalDialog();
        modalDialog.typeInUsername("dino");
        modalDialog.typeInPassword("choochoo");
        modalDialog.clickOnTheLoginButton();
        String greetingsMessage = header.getGreetingsMessage();
        boolean isLogged = greetingsMessage.contains("dino");
        System.out.println("Hi Dino is displayed in the header: " + isLogged);
        System.out.println("Greetings msg is " + greetingsMessage);

        System.out.println("---------------------------------------------------");
        System.out.println("2. User can add product to cart from product cards.");
        homePage.openHomePage();
        ProductCards cards = new ProductCards();
        Product awesomeGraniteChips = cards.getProductByName("Awesome Granite Chips");
        System.out.println("Product is " + awesomeGraniteChips);
        awesomeGraniteChips.clickOnTheProductCartIcon();

        System.out.println(("--------------------------------------------------"));
        System.out.println("3. User can navigate to Home Page from Wishlist Page.");
        homePage.openHomePage();
        header.clickOnTheWhishListIcon();
        String url = header.getUrl();
        System.out.println("Expected to be on the Wishlist page " + url);
        header.clickOnTheShoppingBagIcon();
        url = header.getUrl();
        System.out.println("Expected to be on the Homepage page " + url);

        System.out.println(("--------------------------------------------------"));
        System.out.println("4. User can navigate to Home Page from Cart Page.");
        homePage.openHomePage();
        header.clickOnTheCartIconIcon();
        url = header.getUrl();
        System.out.println("Expected to be on the Cart Page " + url);
        header.clickOnTheShoppingBagIcon();
        url = header.getUrl();
        System.out.println("Expected to be on the Homepage page " + url);



    }
}

// - User can navigate to Home Page from Wishlist Page
// 1. Open Home Page.
// 2. Click on the "favorites" icon.
// 3. Click on the "Shopping bag" icon.
// Expected results: Home Page is loaded.

//----------------
// User can add product to cart from product cards
// 1. Open HomePage .
// 2. Click on the "Awesome Granite Chips" cart icon.
// Expected results: Mini Cart icon shows 1 product in cart.
//

//--------------
// 1. Open Home Page.
// 2. Click on the Login Button.
// 3. Click on the Username Field.
// 4. Type in dino.
// 5. Click on the Password Field.
// 6. Type in choochoo.
// 7. Click on the Login button.
// Expected: Hi dino is displayed in the header.